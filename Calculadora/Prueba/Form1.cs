﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prueba
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSuma_Click(object sender, EventArgs e)
        {
            double v1 = double.Parse(textBoxValor1.Text);
            double v2 = double.Parse(textBoxValor2.Text);
            double resul = v1 + v2;
            labelResul.Text = String.Concat(resul);
        }

        private void buttonResta_Click(object sender, EventArgs e)
        {
            double v1 = double.Parse(textBoxValor1.Text);
            double v2 = double.Parse(textBoxValor2.Text);
            double resul = v1 - v2;
            labelResul.Text = String.Concat(resul);
        }

        private void buttonMulti_Click(object sender, EventArgs e)
        {
            double v1 = double.Parse(textBoxValor1.Text);
            double v2 = double.Parse(textBoxValor2.Text);
            double resul = v1 * v2;
            labelResul.Text = String.Concat(resul);
        }

        private void buttonDivi_Click(object sender, EventArgs e)
        {
            if((double.Parse(textBoxValor2.Text) == 0) || (double.Parse(textBoxValor1.Text)==0)) {
                MessageBox.Show("No se puede dividir entre 0");
                textBoxValor1.Clear();
                textBoxValor2.Clear();
            }
            else {
                double v1 = double.Parse(textBoxValor1.Text);
                double v2 = double.Parse(textBoxValor2.Text);
                double resul = v1 / v2;
                labelResul.Text = String.Concat(resul);
            }
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            textBoxValor1.Clear();
            textBoxValor2.Clear();
            labelResul.Text = "0";
        }
    }
}
