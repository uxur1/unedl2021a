﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tutorial_LINQ
{
    public class Student
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int ID { get; set; }
        public List<int> Calificaciones;
    }

    private static List<Student> students = new List<Student>
    {
        new Student {Nombre = "Daniel", Apellido="Lopez", ID=34, Calificaciones = new List<int>{98,99,100,95}},
        new Student {Nombre = "Fernando", Apellido="Hernandez", ID=546, Calificaciones = new List<int>{89,99,90,96}},
        new Student {Nombre = "Luis", Apellido="Gomez", ID=343, Calificaciones = new List<int>{95,69,100,97}},
        new Student {Nombre = "Angel", Apellido="Duran", ID=12, Calificaciones = new List<int>{96,92,86,76}}
    };
}
