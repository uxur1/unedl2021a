﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace tutorial_LINQ
{
    class Program
    {

        static void Main(string[] args)
        {
            IOrderedEnumerable<Student> studentQuery =
                from student in students
                where student.Calificaciones[0] > 90 && student.Calificaciones[3] < 80
                orderby student.Calificaciones[0] descending
                select student;

            foreach (Student student in studentQuery)
            {
                Console.WriteLine("{0}, {1} {2}", student.Apellido, student.Nombre, student.Calificaciones[0]);
            }

            var studentQuery2 =
                from student in students
                group student by student.Apellido[0];

            foreach (var studentGroup in studentQuery2)
            {
                Console.WriteLine(studentGroup.Key);
                foreach (Student student in studentGroup)
                {
                    Console.WriteLine("   {0}, {1}",
                              student.Apellido, student.Nombre);
                }
            }

            var studentQuery3 =
                from student in students
                group student by student.Apellido[0];

            foreach (var groupOfStudents in studentQuery3)
            {
                Console.WriteLine(groupOfStudents.Key);
                foreach (var student in groupOfStudents)
                {
                    Console.WriteLine("   {0}, {1}",
                        student.Apellido, student.Daniel);
                }
            }

            var studentQuery4 =
                from student in students
                group student by student.Apellido[0] into studentGroup
                orderby studentGroup.Key
                select studentGroup;

            foreach (var groupOfStudents in studentQuery4)
            {
                Console.WriteLine(groupOfStudents.Key);
                foreach (var student in groupOfStudents)
                {
                    Console.WriteLine("   {0}, {1}",
                        student.Apellido, student.Nombre);
                }
            }

            var studentQuery5 =
                from student in students
                let totalScore = student.Calificaciones[0] + student.Calificaciones[1] +
                student.Calificaciones[2] + student.Calificaciones[3]
                where totalScore / 4 < student.Calificaciones[0]
                select student.Apellido + " " + student.Nombre;

            foreach (string s in studentQuery5)
            {
                Console.WriteLine(s);
            }

            var studentQuery6 =
                from student in students
                let totalScore = student.Calificaciones[0] + student.Calificaciones[1] +
                student.Calificaciones[2] + student.Calificaciones[3]
                select totalScore;

            double averageScore = studentQuery6.Average();
            Console.WriteLine("Class average score = {0}", averageScore);

            IEnumerable<string> studentQuery7 =
                from student in students
                where student.Apellido == "Garcia"
                select student.Nombre;

            Console.WriteLine("The Garcias in the class are:");
            foreach (string s in studentQuery7)
            {
                Console.WriteLine(s);
            }

            var studentQuery8 =
                from student in students
                let x = student.Calificaciones[0] + student.Calificaciones[1] +
                student.Calificaciones[2] + student.Calificaciones[3]
                where x > averageScore
                select new { id = student.ID, score = x };

            foreach (var item in studentQuery8)
            {
                Console.WriteLine("Student ID: {0}, Score: {1}", item.id, item.score);
            }

        }
    }
}
