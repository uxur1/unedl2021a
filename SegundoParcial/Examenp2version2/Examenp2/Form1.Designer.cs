﻿
namespace Examenp2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonInstalar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxLoca = new System.Windows.Forms.TextBox();
            this.labelBase = new System.Windows.Forms.Label();
            this.textBoxBase = new System.Windows.Forms.TextBox();
            this.labelComunica = new System.Windows.Forms.Label();
            this.textBoxComunica = new System.Windows.Forms.TextBox();
            this.labelIP = new System.Windows.Forms.Label();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.labelContra = new System.Windows.Forms.Label();
            this.textBoxContra = new System.Windows.Forms.TextBox();
            this.labelRedun = new System.Windows.Forms.Label();
            this.radioButtonNormal = new System.Windows.Forms.RadioButton();
            this.radioButtonHigh = new System.Windows.Forms.RadioButton();
            this.radioButtonLow = new System.Windows.Forms.RadioButton();
            this.buttonFinalizar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonInstalar
            // 
            this.buttonInstalar.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonInstalar.Location = new System.Drawing.Point(294, 172);
            this.buttonInstalar.Name = "buttonInstalar";
            this.buttonInstalar.Size = new System.Drawing.Size(127, 51);
            this.buttonInstalar.TabIndex = 1;
            this.buttonInstalar.Text = "Instalar";
            this.buttonInstalar.UseVisualStyleBackColor = true;
            this.buttonInstalar.Click += new System.EventHandler(this.buttonInstalar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(226, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 47);
            this.label1.TabIndex = 4;
            this.label1.Text = "Examen Parcial 2";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButton1.Location = new System.Drawing.Point(416, 106);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(63, 25);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "FULL";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButton2.Location = new System.Drawing.Point(485, 106);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(63, 25);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "PART";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButton3.Location = new System.Drawing.Point(554, 106);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(58, 25);
            this.radioButton3.TabIndex = 7;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "MIN";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(414, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Opciones de Instalacion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(109, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "Localidad del Inventario";
            // 
            // textBoxLoca
            // 
            this.textBoxLoca.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxLoca.Location = new System.Drawing.Point(109, 106);
            this.textBoxLoca.Name = "textBoxLoca";
            this.textBoxLoca.Size = new System.Drawing.Size(214, 29);
            this.textBoxLoca.TabIndex = 11;
            // 
            // labelBase
            // 
            this.labelBase.AutoSize = true;
            this.labelBase.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelBase.Location = new System.Drawing.Point(74, 309);
            this.labelBase.Name = "labelBase";
            this.labelBase.Size = new System.Drawing.Size(137, 25);
            this.labelBase.TabIndex = 12;
            this.labelBase.Text = "Localidad Base";
            this.labelBase.Visible = false;
            // 
            // textBoxBase
            // 
            this.textBoxBase.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxBase.Location = new System.Drawing.Point(64, 347);
            this.textBoxBase.Name = "textBoxBase";
            this.textBoxBase.Size = new System.Drawing.Size(157, 29);
            this.textBoxBase.TabIndex = 13;
            this.textBoxBase.Visible = false;
            // 
            // labelComunica
            // 
            this.labelComunica.AutoSize = true;
            this.labelComunica.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelComunica.Location = new System.Drawing.Point(32, 403);
            this.labelComunica.Name = "labelComunica";
            this.labelComunica.Size = new System.Drawing.Size(220, 25);
            this.labelComunica.TabIndex = 14;
            this.labelComunica.Text = "Puerto de Comunicacion";
            this.labelComunica.Visible = false;
            // 
            // textBoxComunica
            // 
            this.textBoxComunica.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxComunica.Location = new System.Drawing.Point(64, 444);
            this.textBoxComunica.Name = "textBoxComunica";
            this.textBoxComunica.Size = new System.Drawing.Size(157, 29);
            this.textBoxComunica.TabIndex = 15;
            this.textBoxComunica.Visible = false;
            // 
            // labelIP
            // 
            this.labelIP.AutoSize = true;
            this.labelIP.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelIP.Location = new System.Drawing.Point(308, 403);
            this.labelIP.Name = "labelIP";
            this.labelIP.Size = new System.Drawing.Size(113, 25);
            this.labelIP.TabIndex = 16;
            this.labelIP.Text = "Direccion IP";
            this.labelIP.Visible = false;
            // 
            // textBoxIP
            // 
            this.textBoxIP.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxIP.Location = new System.Drawing.Point(283, 444);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(157, 29);
            this.textBoxIP.TabIndex = 17;
            this.textBoxIP.Visible = false;
            // 
            // labelContra
            // 
            this.labelContra.AutoSize = true;
            this.labelContra.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelContra.Location = new System.Drawing.Point(319, 309);
            this.labelContra.Name = "labelContra";
            this.labelContra.Size = new System.Drawing.Size(91, 25);
            this.labelContra.TabIndex = 18;
            this.labelContra.Text = "Password";
            this.labelContra.Visible = false;
            // 
            // textBoxContra
            // 
            this.textBoxContra.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxContra.Location = new System.Drawing.Point(283, 347);
            this.textBoxContra.Name = "textBoxContra";
            this.textBoxContra.Size = new System.Drawing.Size(157, 29);
            this.textBoxContra.TabIndex = 19;
            this.textBoxContra.Visible = false;
            // 
            // labelRedun
            // 
            this.labelRedun.AutoSize = true;
            this.labelRedun.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelRedun.Location = new System.Drawing.Point(503, 309);
            this.labelRedun.Name = "labelRedun";
            this.labelRedun.Size = new System.Drawing.Size(217, 25);
            this.labelRedun.TabIndex = 20;
            this.labelRedun.Text = "Sistema de Redundancia";
            this.labelRedun.Visible = false;
            // 
            // radioButtonNormal
            // 
            this.radioButtonNormal.AutoSize = true;
            this.radioButtonNormal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonNormal.Location = new System.Drawing.Point(581, 351);
            this.radioButtonNormal.Name = "radioButtonNormal";
            this.radioButtonNormal.Size = new System.Drawing.Size(94, 25);
            this.radioButtonNormal.TabIndex = 21;
            this.radioButtonNormal.TabStop = true;
            this.radioButtonNormal.Text = "NORMAL";
            this.radioButtonNormal.UseVisualStyleBackColor = true;
            this.radioButtonNormal.Visible = false;
            // 
            // radioButtonHigh
            // 
            this.radioButtonHigh.AutoSize = true;
            this.radioButtonHigh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonHigh.Location = new System.Drawing.Point(581, 382);
            this.radioButtonHigh.Name = "radioButtonHigh";
            this.radioButtonHigh.Size = new System.Drawing.Size(65, 25);
            this.radioButtonHigh.TabIndex = 22;
            this.radioButtonHigh.TabStop = true;
            this.radioButtonHigh.Text = "HIGH";
            this.radioButtonHigh.UseVisualStyleBackColor = true;
            this.radioButtonHigh.Visible = false;
            // 
            // radioButtonLow
            // 
            this.radioButtonLow.AutoSize = true;
            this.radioButtonLow.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radioButtonLow.Location = new System.Drawing.Point(581, 413);
            this.radioButtonLow.Name = "radioButtonLow";
            this.radioButtonLow.Size = new System.Drawing.Size(62, 25);
            this.radioButtonLow.TabIndex = 23;
            this.radioButtonLow.TabStop = true;
            this.radioButtonLow.Text = "LOW";
            this.radioButtonLow.UseVisualStyleBackColor = true;
            this.radioButtonLow.Visible = false;
            // 
            // buttonFinalizar
            // 
            this.buttonFinalizar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonFinalizar.Location = new System.Drawing.Point(554, 456);
            this.buttonFinalizar.Name = "buttonFinalizar";
            this.buttonFinalizar.Size = new System.Drawing.Size(126, 48);
            this.buttonFinalizar.TabIndex = 24;
            this.buttonFinalizar.Text = "Finalizar";
            this.buttonFinalizar.UseVisualStyleBackColor = true;
            this.buttonFinalizar.Visible = false;
            this.buttonFinalizar.Click += new System.EventHandler(this.buttonFinalizar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 525);
            this.Controls.Add(this.buttonFinalizar);
            this.Controls.Add(this.radioButtonLow);
            this.Controls.Add(this.radioButtonHigh);
            this.Controls.Add(this.radioButtonNormal);
            this.Controls.Add(this.labelRedun);
            this.Controls.Add(this.textBoxContra);
            this.Controls.Add(this.labelContra);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.labelIP);
            this.Controls.Add(this.textBoxComunica);
            this.Controls.Add(this.labelComunica);
            this.Controls.Add(this.textBoxBase);
            this.Controls.Add(this.labelBase);
            this.Controls.Add(this.textBoxLoca);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonInstalar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonInstalar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxLoca;
        private System.Windows.Forms.Label labelBase;
        private System.Windows.Forms.TextBox textBoxBase;
        private System.Windows.Forms.Label labelComunica;
        private System.Windows.Forms.TextBox textBoxComunica;
        private System.Windows.Forms.Label labelIP;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.Label labelContra;
        private System.Windows.Forms.TextBox textBoxContra;
        private System.Windows.Forms.Label labelRedun;
        private System.Windows.Forms.RadioButton radioButtonNormal;
        private System.Windows.Forms.RadioButton radioButtonHigh;
        private System.Windows.Forms.RadioButton radioButtonLow;
        private System.Windows.Forms.Button buttonFinalizar;
    }
}

