﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examenp2
{
    public partial class Form1 : Form
    {
        string localidad, opcIns, locaBase, puerto, ip, password, redun;
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonInstalar_Click(object sender, EventArgs e)
        {
            using (FileStream archivo = File.Create("C:/Users/danie/source/repos/Examenp2/Examenp2/" + textBoxLoca.Text + ".txt"))
            {
                MessageBox.Show("Archivo Creado");
            }

            if (radioButton1.Checked == true)
            {
                textBoxLoca.Enabled = false;
                buttonInstalar.Enabled = false;
                labelBase.Visible = true;
                textBoxBase.Visible = true;
                labelComunica.Visible = true;
                textBoxComunica.Visible = true;
                labelContra.Visible = true;
                textBoxContra.Visible = true;
                labelIP.Visible = true;
                textBoxIP.Visible = true;
                labelRedun.Visible = true;
                radioButtonNormal.Visible = true;
                radioButtonHigh.Visible = true;
                radioButtonLow.Visible = true;
                buttonFinalizar.Visible = true;
                
            }
            if (radioButton2.Checked == true)
            {
                buttonInstalar.Enabled = false;
                labelBase.Visible = true;
                textBoxBase.Visible = true;
                labelRedun.Visible = true;
                radioButtonNormal.Visible = true;
                radioButtonHigh.Visible = true;
                radioButtonLow.Visible = true;
                labelContra.Visible = true;
                textBoxContra.Visible = true;
                buttonFinalizar.Visible = true;
            }
            if (radioButton3.Checked == true)
            {
                buttonInstalar.Enabled = false;
                labelBase.Visible = true;
                textBoxBase.Visible = true;
                labelRedun.Visible = true;
                radioButtonNormal.Visible = true;
                radioButtonHigh.Visible = true;
                radioButtonLow.Visible = true;
                labelContra.Visible = true;
                textBoxContra.Visible = true;
                buttonFinalizar.Visible = true;
            }


        }

        private void buttonFinalizar_Click(object sender, EventArgs e)
        {
            StreamWriter archivo = new StreamWriter("C:/Users/danie/source/repos/Examenp2/Examenp2/" + textBoxLoca.Text + ".txt");
            if (radioButtonNormal.Checked == true)
            {
                MessageBox.Show("Tu instalacion se hizo con redundancia NORMAL");
                if (radioButton1.Checked == true)
                {
                    
                    opcIns = "Tipo de instalacion: Instalacion Total";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "Puerto de Comunicacion: " + textBoxComunica.Text;
                    ip = "Direccion Ip: " + textBoxIP.Text;
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: NORMAL";
                }
                if (radioButton2.Checked == true)
                {
                    opcIns = "Tipo de instalacion: Instalacion Parcial";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "";
                    ip = "";
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: HIGH";

                    
                }
                if (radioButton3.Checked == true)
                {
                    opcIns = "Tipo de instalacion: Instalacion Minima";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "";
                    ip = "";
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: LOW";
                }
                archivo.WriteLine(opcIns);
                archivo.WriteLine(localidad);
                archivo.WriteLine(locaBase);
                archivo.WriteLine(puerto);
                archivo.WriteLine(ip);
                archivo.WriteLine(password);
                archivo.WriteLine(redun);
                archivo.Close();
            }
            if (radioButtonHigh.Checked == true)
            {
                MessageBox.Show("Tu instalacion se hizo con redundancia HIGH");
                if (radioButton1.Checked == true)
                {

                    opcIns = "Tipo de instalacion: Instalacion Total";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "Puerto de Comunicacion: " + textBoxComunica.Text;
                    ip = "Direccion Ip: " + textBoxIP.Text;
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: NORMAL";
                }
                if (radioButton2.Checked == true)
                {
                    opcIns = "Tipo de instalacion: Instalacion Parcial";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "";
                    ip = "";
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: HIGH";


                }
                if (radioButton3.Checked == true)
                {
                    opcIns = "Tipo de instalacion: Instalacion Minima";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "";
                    ip = "";
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: LOW";
                }
                archivo.WriteLine(opcIns);
                archivo.WriteLine(localidad);
                archivo.WriteLine(locaBase);
                archivo.WriteLine(puerto);
                archivo.WriteLine(ip);
                archivo.WriteLine(password);
                archivo.WriteLine(redun);
                archivo.Close();
            }
            if (radioButtonLow.Checked == true)
            {
                MessageBox.Show("Tu instalacion se hizo con redundancia LOW");
                if (radioButton1.Checked == true)
                {

                    opcIns = "Tipo de instalacion: Instalacion Total";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "Puerto de Comunicacion: " + textBoxComunica.Text;
                    ip = "Direccion Ip: " + textBoxIP.Text;
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: NORMAL";
                }
                if (radioButton2.Checked == true)
                {
                    opcIns = "Tipo de instalacion: Instalacion Parcial";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "";
                    ip = "";
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: HIGH";


                }
                if (radioButton3.Checked == true)
                {
                    opcIns = "Tipo de instalacion: Instalacion Minima";
                    localidad = "Nombre de archivo: " + textBoxLoca.Text + ".txt";
                    locaBase = "Localidad Base: " + textBoxBase.Text;
                    puerto = "";
                    ip = "";
                    password = "Contraseña: " + textBoxContra.Text;
                    redun = "Sistema de Redundancia: LOW";
                }
                archivo.WriteLine(opcIns);
                archivo.WriteLine(localidad);
                archivo.WriteLine(locaBase);
                archivo.WriteLine(puerto);
                archivo.WriteLine(ip);
                archivo.WriteLine(password);
                archivo.WriteLine(redun);
                archivo.Close();
            }
            
        
        }
    }
}
